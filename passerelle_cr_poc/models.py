# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
import psycopg2

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError


class ReunionConnector(BaseResource):

    server = models.CharField(max_length=128, verbose_name=_('DB server'))
    username = models.CharField(max_length=128, verbose_name=_('DB user'))
    password = models.CharField(max_length=128, verbose_name=_('DB passsword'), null=True, blank=True)
#    dbname = models.CharField(max_length=128, verbose_name=_('DB name'), null=True)
    
    category = 'Divers'

    class Meta:
        verbose_name = u'Connecteur de test réunion'

    @endpoint(
        description_get=_('Ping'), methods=['get'], perm='can_access',
        parameters={
            'piment': {
                'description': _('Piment'),
                'example_value': 'oui',
            },
        }
    )
    def ping(self, request, piment):
        return {'data': piment}

    # @endpoint(description_get=_('Paiements'), methods=['get'], perm='can_access')
    # def paiements(self, request):
    #     conn = psycopg2.connect(
    #         "host=%s port=5432 dbname=%mafat user=%s password=%s" % (
    #             self.server,
    #             self.username,
    #             self.password)
    #     )
    #     cur = conn.cursor()
    #     cur.execute('''SELECT id, demarche_id, email, paiement
    #                      FROM chequenumerique
    #                 ''')

    #     data = []
    #     while True:
    #         t = cur.fetchone()
    #         if not t:
    #             break
    #         id_, demarche_id, email, paiement = t
    #         data.append({
    #             'id': id_,
    #             'text': demarche_id,
    #             'email': email,
    #             'paiement': paiement
    #         })

    #     return {'data': data}
